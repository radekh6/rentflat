includeTargets << grailsScript("_GrailsInit")
includeTargets << grailsScript("_GrailsBootstrap")

includeTargets << new File(databaseMigrationPluginDir, 'scripts/DbmDropAll.groovy')
includeTargets << new File(databaseMigrationPluginDir, 'scripts/DbmUpdate.groovy')
includeTargets << new File(databaseMigrationPluginDir, 'scripts/DbmGormDiff.groovy')


target('generate-migration': "Generate socialcom DB migation scripts ready for release") {
    def logPrefix = "SocialCom:"
    grailsConsole.info "$logPrefix build socialcom: args map is: $argsMap"

    //configureProxy, packageApp, classpath,
    depends(clean, compile)

    /*
     * TODO: SVN Update to head
     */

    grailsConsole.info "$logPrefix removing jdbcStore from quartz props"

   /* propertyfile(file: "grails-app/conf/quartz.properties") {
        entry(key: "grails.plugin.quartz2.jdbcStore", operation: "del")
    }*/

    grailsConsole.info "$logPrefix calling db migration drop all (argsMap $argsMap)"
    // includes a depend dbmInit
    dbmDropAll()


    grailsConsole.info "$logPrefix calling db migration update (argsMap $argsMap)"
    dbmUpdate()


    def nextVersion = 'unknown'
    if (grailsApp && grailsApp?.metadata) {
        nextVersion = grailsApp?.metadata['app.version']
    }
    grailsConsole.info "$logPrefix build socialcom Release: v.${nextVersion}  ( grails metadata: ${grailsApp?.metadata})"

    def newXmlFile = "changelog-${nextVersion}.xml"
    def argsListForGormDiff = ["${newXmlFile}".toString()]
    argsMap.putAt("params", argsListForGormDiff)
    /*
     * the dbmInit has already read out the argsMap and put it into a variable argsList
     * For this reason, we need to set argsList now (NOT argsMap)
     */
    argsList = argsListForGormDiff
    grailsConsole.info "$logPrefix calling db migration gorm diff: argsMap $argsMap argsList: $argsList"

    dbmGormDiff()

    /*
     * add xml migration to changelog.xml
     */
    grailsConsole.info "$logPrefix updating changelog.xml..."

    def newMigration = "<include file=\"${newXmlFile}\"/>"
    grailsConsole.info "$logPrefix adding:\n$newMigration"

    ant.replace(file: "grails-app/migrations/changelog.xml") {
        replacefilter(token: "</databaseChangeLog>", value: "\t" + newMigration + "\n" + "</databaseChangeLog>")
    }

    grailsConsole.info "$logPrefix adding jdbcStore=true to quartz props"

    propertyfile(file: "grails-app/conf/quartz.properties") {
        entry(key: "grails.plugin.quartz2.jdbcStore", value: "true", operation: "=")
    }

    /*
     * TODO: result of migration update is only printed to the console and NOT written to a file. Also, the changelog.groovy is NOT updated
     * TODO: SVN commit
     * TODO: SVN branch/tag
     * TODO: grails clean
     * TODO: grails prod war
     */
}

setDefaultTarget("generate-migration")
