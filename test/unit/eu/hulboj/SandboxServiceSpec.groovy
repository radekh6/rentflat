package eu.hulboj

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by radoslaw on 11.01.16.
 */
@TestFor(SandboxService)
class SandboxServiceSpec extends Specification {

    def setup() {
        test 'Polska'
    }

    @Unroll
    def "Only the 2 NL: return 2 BR"() {

        when: ''
        def html = service.decorateHtmlWithSpan(testData)

        then:''
        html == result

        where:''
        testData        | result
        "\n:)\n"        | "</br><span>:) </span></br>"
        ":)\n"          | "<span>:) </span></br>"
        ":)"            | "<span>:)</span>"
        "\n\n:):)\n\n"  | "</br></br><span>:):) </span></br></br>"
        ":):)\n\n\n"    | "<span>:):) </span></br></br></br>"
        ":):)"          | "<span>:):)</span>"
        ":) :)"         | "<span>:) :)</span>"
        ":)\n\n"        | "<span>:) </span></br></br>"
        "\n\n"          | "</br></br>"

    }

    private test (String par) {
        println par
    }
}
