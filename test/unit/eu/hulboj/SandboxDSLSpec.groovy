package eu.hulboj

import spock.lang.Specification

public class Box {
    public def passMapAsParameter(def transform) {
        transform.a + transform.b
    }
    public Box whenBox(Closure c){
        c("Nimecy")
        this
    }
    public void thenBox(Closure c) {
        c("Polska")
    }
    public def passMap(def mapowanie, Closure c) {
        def ile = c()
        println mapowanie.a
    }
}
class SandboxDSLSpec extends Specification {
    def 'teas' () {
        given:
        Box box = new Box()
        box.whenBox { panstwo ->
            println(panstwo)
        } thenBox {
            println(it)
        }
        box.passMap(a:'dd') {
            a: 'polska'
            b: 'oni'
        }
        [].collect()
        when:
        def res = box.passMapAsParameter a:1, b:2
        then: ''
        res  == 3
    }
}
