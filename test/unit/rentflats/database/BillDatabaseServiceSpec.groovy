package rentflats.database

import eu.hulboj.form.Amount
import grails.buildtestdata.mixin.Build
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import rentflats.json.JsonAmountService
import spock.lang.Specification

@TestFor(JsonAmountService)
@Mock(Amount)
@Build(Amount)
class BillDatabaseServiceSpec extends Specification {


    def "json is valid"() {
        given:
        def factors = [
            gas : 1,
            water: 2,
            current1 : 3
        ]

        when:
        def result = service.hasValidBillJson(factors)

        then:
        result == true
        notThrown RuntimeException
    }

    def "json is invalid has wrong format"() {
        given:
        def factors = [
                gas : 'not number',
                water: 3,
                current1 : 3
        ]

        when:
        def result = service.hasValidBillJson(factors)

        then:
        result == null
        thrown RuntimeException
    }

}
