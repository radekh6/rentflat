package rentflats.json

import grails.test.mixin.TestFor
import groovy.json.JsonSlurper
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(JsonPaymentService)
class JsonPaymentServiceSpec extends Specification {

    def setup() {

    }

    def cleanup() {
    }

    void "The json key are proper and values are integers"() {
        given:
        String json = '{"current2":"1","current1":"2","gas":"2","water":"55"}'
        def jsonStructure = new JsonSlurper().parseText(json)

        when:
        boolean  res = service.hasValidPaymentJson(jsonStructure)

        then:
        res == true

    }

    void "The json key are proper and values are not integers"() {
        given:
        String json = '{"current2":"a","current1":"b","gas":"2","water":"55"}'
        def jsonStructure = new JsonSlurper().parseText(json)

        when:
        boolean  res = service.hasValidPaymentJson(jsonStructure)

        then:
        res == false
    }
}
