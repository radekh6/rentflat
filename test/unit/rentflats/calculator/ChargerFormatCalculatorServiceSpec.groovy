package rentflats.calculator

import eu.hulboj.Calculator.ConstFactor
import eu.hulboj.Calculator.VariableFactor
import eu.hulboj.config.PayInfo
import eu.hulboj.config.PayType
import eu.hulboj.form.Amount
import eu.hulboj.form.Record
import grails.test.mixin.TestFor
import rentflats.database.FactorDatabaseService
import spock.lang.Specification

/**
 * Created by radoslaw on 17.10.16.
 */
@TestFor(ChargerFormatCalculatorService)
class ChargerFormatCalculatorServiceSpec extends Specification {

    def recordValue = 1
    def variableValue = 2
    def constValue = 3

    public def 'One record in amount'() {
        given:
        def amount = buildAmount()
        mockFactorDatabaseService()
        when:
        String result = service.calculateChargeForBill(amount, 1)

        then:
        result.contains("${constValue + (variableValue * recordValue)}") == true
    }

    private void mockFactorDatabaseService() {
        service.factorDatabaseService = Mock(FactorDatabaseService)
        service.factorDatabaseService.getVariableFactor(_,_) >> { [name: "gas variable", price: variableValue] as VariableFactor}
        service.factorDatabaseService.getConstFactor(_,_) >> { [name: "gas const", price: constValue] as ConstFactor}
        service.chargerCalculatorService = Mock(ChargerCalculatorService)
        service.chargerCalculatorService.calculateChargeForBill(_,_) >> constValue + (variableValue * recordValue)
    }

    private def buildAmount() {
        def amount = [
                records: [
                        payInfo: [
                                payType: PayType.GAS_QUANTITY
                        ] as PayInfo,
                        value: recordValue
                ] as Record
        ] as Amount
        amount
    }
}
