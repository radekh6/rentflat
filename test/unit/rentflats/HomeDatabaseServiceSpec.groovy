package rentflats

import grails.test.mixin.TestFor
import rentflats.database.HomeDatabaseService
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(HomeDatabaseService)
class HomeDatabaseServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
}
