package rentflats

import eu.hulboj.form.Amount
import eu.hulboj.form.Record
import grails.test.mixin.TestFor
import rentflats.calculator.ChargerCalculatorService
import rentflats.database.FactorDatabaseService
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ChargerCalculatorService)
class ChargerCalculatorServiceSpec extends Specification {


    def setup() {
        service.factorDatabaseService = Stub(FactorDatabaseService)
    }

    def cleanup() {
    }

    void "calculate monthly charge for first month in the system"() {
        given:
        Amount amount = new Amount()
        Record record = new Record(value: 3)
        amount.records = []
        amount.records << record

        and:
        service.factorDatabaseService.getVariableFactor(_,_) >> [price: 1]
        service.factorDatabaseService.getConstFactor(_,_) >> [price: 2]

        when:
        Integer charge = service.calculateChargeForBill(amount, 1)

        then:
        charge == 2 + 1 * 3

    }

    void "calculate monthly charge for any next month in the system"() {
    }


}
