package eu.hulboj

import eu.hulboj.Calculator.Agreement
import eu.hulboj.Calculator.ConstFactor
import eu.hulboj.config.Factor
import eu.hulboj.Calculator.Period
import eu.hulboj.Calculator.Provider
import eu.hulboj.Calculator.VariableFactor
import eu.hulboj.config.FactorType
import eu.hulboj.config.PayInfo
import eu.hulboj.config.PayType
import eu.hulboj.config.ProviderInfo
import eu.hulboj.config.ServiceType
import spock.lang.Specification

class IntegrationHomeSpec extends Specification {
    ProviderInfo providerInfo = null
    PayInfo payInfoQuantity = null

    def 'Create Home for Opole flat' () {
        given:
        Home home = buildHome()
        home.addToAgreements constructAgreement()
        home.addToProviderInfos constructProviderInfo()

        when:
        home.save()

        then:
        Home.findByName ('WANDY_TEST') == home
        Tenant.findByName ('Katrina') != null
        /*Agreement.list().size() == 1
        Period.list().size() == 1
        Provider.list().size() == 1
        Factor.list().size() == 2*/

    }

    private def constructAgreement() {
        Agreement agreement = buildAgreement()
        agreement.addToTenats buildTenant()
        agreement.addToPeriods buildPeriod()
        assert agreement.validate()
        agreement
    }

    private def constructProviderInfo() {
        providerInfo = buildProviderInfoWater()
        PayInfo payInfoSubscription = new PayInfo(payType: PayType.WATER_SUBSCRIPTION)
        payInfoQuantity = buildInfoQuantity ()
        providerInfo.addToPayInfo(payInfoSubscription)
        providerInfo.addToPayInfo(payInfoQuantity)
        providerInfo
    }

    private Period buildPeriod() {
        Period period = new Period()
        period.addToProviders buildProvider()
        assert period.validate()
        period
    }

    private Provider buildProvider() {
        Provider waterProvider = new Provider(name: 'Wodowe przedsiebiorstwo wodne')
        waterProvider.providerInfo = buildProviderInfoWater()
        waterProvider.address = buildAddress()
        waterProvider.addToFactors buildConstFactor()
        waterProvider.addToFactors buildVariableFactor()
        assert waterProvider.validate()
        waterProvider
    }


    private VariableFactor buildVariableFactor() {
        VariableFactor quantityFactor = new VariableFactor(price: 10, unit: 'zl', name: 'subscription')
        quantityFactor.factorType = FactorType.Water
        VariableFactor variableFactor = quantityFactor
        variableFactor.payInfo = buildInfoQuantity()
        variableFactor
    }

    private ConstFactor buildConstFactor() {
        ConstFactor constFactor = new ConstFactor(price: 10, unit: 'zl', name: 'subscription')
        constFactor.factorType = FactorType.Water
        constFactor
    }

    private Address buildAddress() {
        Address address = new Address(city: 'Opole', street: 'Wodna')
        assert address.validate()

        address.save(failOnError: true)
        address
    }

    private Agreement buildAgreement() {
        File scan = new File('agreement.txt')
        Agreement agreement = new Agreement(scanAgreement: scan)
        agreement
    }

    private Tenant buildTenant() {
        Tenant tenant = new Tenant(name: 'Katrina', furName: 'Torbicka', userName: 'torba')
        assert tenant.validate()
        tenant
    }

    private ProviderInfo buildProviderInfoWater() {
        if(providerInfo == null ) {
            providerInfo = new ProviderInfo(serviceType: ServiceType.WATER)
        }
        providerInfo
    }

    private Home buildHome() {
        Address address = new Address(city:'Opole',street: 'Wandy')
        address.save(failOnError: true)
        new Home(address: address, name:'WANDY_TEST')
    }

    private def buildInfoQuantity () {
        if (payInfoQuantity == null) {
            payInfoQuantity  = new PayInfo(payType: PayType.WATER_QUANTITY)
        }
        payInfoQuantity
    }
}
