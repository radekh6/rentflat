package eu.hulboj

import grails.test.spock.IntegrationSpec
import org.joda.time.DateTime

class AgreementDatabaseServiceIntegrationSpec extends IntegrationSpec {

    AgreementService agreementService


    def setup() {
    }

    def cleanup() {
    }

    void "Saving agreement with file and DateTime"() {
        given:''
        DateTime sd = new DateTime()
        DateTime ed = new DateTime().plusDays(1)
        def file = new File("test/integration/agreementFile.txt")

        when:''
        agreementService.saveAgreement(sd, ed, file)

        then:''
        notThrown Exception
    }
}
