package eu.hulboj

import spock.lang.Specification

/**
 * Created by radoslaw on 21.09.16.
 */
class FlatSpec extends Specification {
    def 'Build Flat for testing '() {
        given: ''
        Home home = new Home(name: 'Opole-Wandy')
        Address address = new Address(street: 'ul. Wandy', city: 'Opole')
        home.address = address

        when: ''
        address.save(failOnError: true)
        home.save(failOnError: true)

        then: ''
        Home.findByName( 'Opole-Wandy') != null
        Address.findByStreet ('ul. Wandy') != null
    }
}
