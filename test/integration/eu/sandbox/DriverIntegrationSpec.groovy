package eu.sandbox

import grails.test.spock.IntegrationSpec
import org.springframework.dao.InvalidDataAccessApiUsageException

class DriverIntegrationSpec extends IntegrationSpec {


    def setup() {
    }

    def cleanup() {
    }

    void "Many to one from Driver to Car. The car belongs to driver, no back relation: Automatically save all on driver. The back reference is set"() {

        given: 'One driver and one car'
        Driver driver = new Driver()
        driver.name = "Radek"
        driver.car = new Car()
        driver.car.name = "Porche"

        when:
        driver.save(failOnError: true)

        then: 'The back reference of the car is set automatically'
        assert  Driver.findAll().size() == 1
        assert  Car.findAll().size() == 1
        Car.findAll()[0].driver == Driver.findAll()[0]
    }

    void "Many to one from Driver to Car. The car belongs to driver, no back relation: Automatically delete all on driver"() {

        given: 'One driver and one car'
        Driver driver = new Driver()
        driver.name = "Radek"
        driver.car = new Car()
        driver.car.name = "Porche"

        when:
        driver.save(failOnError: true)
        driver.delete(flush: true)

        then:
        assert  Driver.findAll().size() == 0
        assert  Car.findAll().size() == 0
    }

    void "2 Drivers owns 1 Car and one driver delete!!! Many to one from Driver to Car. The car belongs to driver, no back relation: Automatically delete all on driver"() {

        given: 'One driver and one car'
        Car theSameCar = new Car()
        theSameCar.name = "Porche"

        Driver driver1 = new Driver()
        driver1.name = "Radek"
        driver1.car = theSameCar
        Driver driver2 = new Driver()
        driver2.name = "Radek"
        driver2.car = theSameCar

        when:
        driver1.save(failOnError: true)
        driver2.save(failOnError: true)
        driver1.car = null
        driver1.delete(flush: true)

        then:
        assert  Driver.findAll().size() == 1
        assert  Car.findAll().size() == 1
    }

    void "Pair: 1 Driver owns 1 Car and one driver delete!!! Many to one from Driver to Car. The car belongs to driver, no back relation: Automatically delete all on driver"() {

        given: 'One driver and one car'

        Driver driver1 = new Driver()
        driver1.name = "Radek"
        driver1.car = new Car()
        driver1.car.name = "Porche"

        Driver driver2 = new Driver()
        driver2.name = "Matti"
        driver2.car = new Car()
        driver2.car.name = "Brava"

        when:
        driver1.save(failOnError: true)
        driver2.save(failOnError: true)
        driver1.delete(flush: true)

        then:
        assert  Driver.findAll().size() == 1
        assert  Car.findAll().size() == 1
    }

    void "You can't  delete child in many to one relation when parent owns child !!!" () {

        given:""
        Driver driver = new Driver()
        driver.name = "Radek"
        driver.car = new Car()
        driver.car.name = "Porsche"
        driver.save(failOnError: true)

        when:

        driver.car.discard()
        driver.car = null

        driver.save(failOnError: true)

        then:
        assert Driver.findAll().size() == 1
        assert Car.findAll().size() == 1
    }

    void "Error 2 Drivers owns 1 Car and one driver delete!!! Many to one from Driver to Car. The car belongs to driver, no back relation: Automatically delete all on driver"() {

        given: 'One driver and one car'
        Car theSameCar = new Car()
        theSameCar.name = "Porsche"

        Driver driver1 = new Driver()
        driver1.name = "Radek"
        driver1.car = theSameCar

        Driver driver2 = new Driver()
        driver2.name = "Matti"
        driver2.car = theSameCar

        when: "no set to driver1.car = null"
        driver1.save(failOnError: true)
        driver2.save(failOnError: true)
        driver1.delete(flush: true)

        then:
        thrown InvalidDataAccessApiUsageException
    }
}
