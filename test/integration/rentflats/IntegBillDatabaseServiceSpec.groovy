package rentflats

import eu.hulboj.form.Amount
import org.joda.time.DateTime
import rentflats.database.BillDatabaseService
import spock.lang.Specification


/**
 * Created by radoslaw on 04.09.16.
 */
class IntegBillDatabaseServiceSpec extends Specification {

    BillDatabaseService billDatabaseService

    def 'The dates with the same month and year and time'() {
        given:
        createBillForDate(2016, 12, 1,  1,0,0, 1)
        createBillForDate(2016, 12, 2,  1,0,0, 3)
        createBillForDate(2016, 12, 30, 1,0,0, 2)

        when:
        def res = billDatabaseService.billsByMonths()

        then:
        res[2].water == 2
        res.size() == 3

    }

    def 'The dates with the same year and time'() {

        given:
        createBillForDate(2016, 11, 30, 1,0,1, 3)
        createBillForDate(2016, 12, 1,  1,0,0, 2)
        createBillForDate(2016, 12, 1,  1,0,1, 1)



        when:
        def res = billDatabaseService.billsByMonths()

        then:
        res[0].water == 3
        res[1].water == 1
        res.size() == 2
    }

    def 'The dates with the same month and year: and day and differ time'() {
        given:
        createBillForDate(2016, 12, 1, 1,0,0, 1)
        createBillForDate(2016, 12, 1, 2,0,0, 2)
        createBillForDate(2016, 12, 1, 3,0,0, 3)


        when:
        def res = billDatabaseService.billsByMonths()

        then:
        res[0].time.toString() == "03:00:00"
        res[0].water == 3
        res.size() == 1
    }

    private Amount createBillForDate(int year, int month, int day, int hour, int min, int sec, int water) {
        Amount bill = new Amount()
        bill.billMonth = new DateTime().withDate(year, month, day).withTime(hour,min,sec,0)
        bill.water = water
        bill.gas = 2
        bill.current1 = 3
        bill.calculatedCharge = 111
        bill.save(failOnError: true)
    }

}
