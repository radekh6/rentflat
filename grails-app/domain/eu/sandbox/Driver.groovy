package eu.sandbox

class Driver {
    String name
    Car car
    // static mapping = {car cascade: "all-delete-orphan"} This won't work for Many to One relation

    static constraints = {
        car(nullable: true)
    }
}
