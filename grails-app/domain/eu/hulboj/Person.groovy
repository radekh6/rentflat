package eu.hulboj

class Person {

    String name
    String furName
    String userName

    Address address
    static embedded = ['address']
    static constraints = {
        address(nullable: true)
        furName(nullable: true)
        userName(nullable: true)
    }
}
