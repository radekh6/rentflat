package eu.hulboj



class Tenant extends Person {

	List<Payment> payments
	static hasMany = [payments : Payment]
    static constraints = {
        payments(nullable: true)
    }
}
