package eu.hulboj

import eu.hulboj.form.Amount
import org.joda.time.DateTime


class Payment {
	public enum StatusEnum { unpaid, paid, pending, accepted }
	
	DateTime dateTime
	StatusEnum status = StatusEnum.unpaid

	static hasOne = [Amount]
	static belongsTo = Tenant
    static constraints = {
		status (nullable:false)
    }
}
