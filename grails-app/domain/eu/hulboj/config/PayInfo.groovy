package eu.hulboj.config

/**
 * Created by radoslaw on 15.09.16.
 */
class PayInfo {
    PayType payType
    static belongsTo = [ProviderInfo]
    static constraints = {
        payType blank: false
    }
}
