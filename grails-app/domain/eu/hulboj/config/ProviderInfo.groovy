package eu.hulboj.config

import eu.hulboj.Home

/**
 * Created by radoslaw on 15.09.16.
 */
class ProviderInfo {
    ServiceType serviceType
    static belongsTo = [Home]
    static hasMany = [payInfo: PayInfo]
    static constraints = {
        serviceType blank: false
    }
}
