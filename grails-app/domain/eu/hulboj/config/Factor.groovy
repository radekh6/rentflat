package eu.hulboj.config

import eu.hulboj.Calculator.Provider

class Factor {

    Double price
    String unit
    String name
    FactorType factorType

    static belongsTo = Provider
    static constraints = {
        unit(nullable: false)
        price(nullable: false)
        name(nullable: false)
    }
}
