package eu.hulboj.config

/**
 * Created by radoslaw on 15.09.16.
 */
enum ServiceType {
    GAS     (1, 'PGING company'),
    WATER   (2, 'Utility water company'),
    CURRENT (3, 'Tauron company')

    int id
    String description

    private ServiceType(int id, String description) {
        this.id = id
        this.description = description
    }

    String toString() { description }
    String getKey() { id }

}