package eu.hulboj.config

/**
 * Created by radoslaw on 09.10.16.
 */
enum FactorType {
    Gas(1, 'GAS'),
    Current1(2, 'Current1'),
    Current2(3, 'Current2'),
    Water(4, 'Water')

    int id
    String description

    FactorType(int id, String description) {
        this.id = id
        this.description = description
    }

    String getKey() { id }
}