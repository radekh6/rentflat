package eu.hulboj.config

/**
 * Created by radoslaw on 15.09.16.
 */
public enum PayType {
    GAS_SUBSCRIPTION    (10, 'gas'),
    GAS_QUANTITY        (11, 'gas quantity'),
    CURRENT_DAY         (20, 'current day'),
    CURRENT_NIGHT       (30, 'current night'),
    WATER_SUBSCRIPTION  (40, 'water'),
    WATER_QUANTITY      (50, 'water quantity')


    int id
    String description

    private PayType(int id, String description) {
        this.id = id
        this.description = description
    }

    String toString() { description }
    String getKey() { id }
}
