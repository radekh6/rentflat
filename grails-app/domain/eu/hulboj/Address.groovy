package eu.hulboj

/**
 * Created by radoslaw on 23.11.15.
 */
class Address {
    String city
    String street
    Integer homeNumber
    String zipCode

    static constraints = {
        zipCode(nullable: true)
        homeNumber(nullable: true)
    }
}
