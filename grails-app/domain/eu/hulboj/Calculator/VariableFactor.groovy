package eu.hulboj.Calculator

import eu.hulboj.config.Factor
import eu.hulboj.config.PayInfo

/**
 * Created by radoslaw on 20.09.16.
 */
class VariableFactor extends Factor {
    PayInfo payInfo
    static hasOne = [PayInfo]
}
