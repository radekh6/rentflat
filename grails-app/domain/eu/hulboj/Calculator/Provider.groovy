package eu.hulboj.Calculator

import eu.hulboj.Address
import eu.hulboj.config.Factor
import eu.hulboj.config.ProviderInfo

class Provider {

	String name
	Address address

	List<Factor> factors
	ProviderInfo providerInfo
	static hasOne = [ProviderInfo]
	static belongsTo = Period
	static hasMany = [factors: Factor]
    static constraints = {
		address(nullable: true)
    }
}
