package eu.hulboj.Calculator
/**
 * Created by radoslaw on 20.09.16.
 */
class Period {

    Date startDate
    Date endDate

    static hasMany = [providers: Provider]
    static belongsTo = [Agreement]
    static constraints = {
        startDate(nullable: true)
        endDate(nullable: true)
        providers(nullable: true)
    }
}
