package eu.hulboj

import eu.hulboj.Calculator.Agreement
import eu.hulboj.config.ProviderInfo

class Home {
    String name
    Address address
    List<Agreement> agreements
    List<ProviderInfo> providerInfos
    static hasMany = [agreements: Agreement, providerInfos: ProviderInfo]
    static constraints = {
        name(unique: true)
        agreements(nullable: true)
        address(nullable: false)
        providerInfos(nullable: true)
    }
}
