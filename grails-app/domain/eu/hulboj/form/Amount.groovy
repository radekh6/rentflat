package eu.hulboj.form

import eu.hulboj.Payment
import org.jadira.usertype.dateandtime.joda.PersistentDateTime
import org.joda.time.DateTime

class Amount {

    DateTime billMonth
    Integer water
    Integer gas
    Integer current1
    Integer current2
    Integer calculatedCharge
    String  calculatedChargeFormat
    static transient DateTime month

    static belongsTo = [Payment]
    static  hasMany = [records: Record]

    static constraints = {
        billMonth(unique: true,nullable: false)
        water(nullable: false)
        gas(nullable: false)
        current1(nullable: false)
        current2(nullable: true)
        calculatedCharge(nullable: false)
        calculatedChargeFormat(nullable: true, size: new IntRange(0,1000))
    }

    static mapping = {
        billMonth type: PersistentDateTime
        month formula: 'MONTH(bill_month)'
    }
}
