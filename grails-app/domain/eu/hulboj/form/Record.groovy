package eu.hulboj.form

import eu.hulboj.config.PayInfo
import eu.hulboj.config.ProviderInfo

/**
 * Created by radoslaw on 20.09.16.
 */
class Record {
    Double value
    PayInfo payInfo
    ProviderInfo providerInfo
    static belongsTo = [Amount]
    static hasOne = [PayInfo, ProviderInfo]
}
