import eu.hulboj.Address
import eu.hulboj.Calculator.Agreement
import eu.hulboj.Calculator.ConstFactor
import eu.hulboj.config.Factor
import eu.hulboj.Calculator.Period
import eu.hulboj.Calculator.Provider
import eu.hulboj.Calculator.VariableFactor
import eu.hulboj.Home
import eu.hulboj.Tenant
import eu.hulboj.config.FactorType
import eu.hulboj.config.PayInfo
import eu.hulboj.config.PayType
import eu.hulboj.config.ProviderInfo
import eu.hulboj.config.ServiceType
import eu.hulboj.security.SecRole
import eu.hulboj.security.SecUser
import eu.hulboj.security.SecUserSecRole

class BootStrap {

	def init = { servletContext ->
		//
		def userRole = SecRole.findByAuthority('ROLE_USER') ?: new SecRole(authority: 'ROLE_USER').save(failOnError: true)
		def normalUser = SecUser.findByUsername('normal') ?: new SecUser(
				username: 'normal',
				password: 'normal',		// RaHu: done automatically -> springSecurityService.encodePassword('normal'),
				enabled: true).save(failOnError: true)

		if (!normalUser.authorities.contains(userRole)) {
			SecUserSecRole.create normalUser, userRole
		}
		//
		def adminRole = SecRole.findByAuthority('ROLE_ADMIN') ?: new SecRole(authority: 'ROLE_ADMIN').save(failOnError: true)
		def adminUser = SecUser.findByUsername('admin') ?: new SecUser(
				username: 'admin',
				password: 'admin', // RaHu: done automatically -> springSecurityService.encodePassword('admin'),
				enabled: true).save(failOnError: true)

		if (!adminUser.authorities.contains(adminRole)) {
			SecUserSecRole.create adminUser, adminRole
		}

		buildPaymentConfigurator();
	}

	private void buildPaymentConfigurator() {
		Home home = buildHome()
		home.addToAgreements constructAgreement()
		home.addToProviderInfos constructWaterProviderInfo()
		home.addToProviderInfos constructGasProviderInfo()
		home.addToProviderInfos constructCurrent1ProviderInfo()
		if(home.validate()) {
			home.save(failOnError: true)
		}
	}

	private def constructAgreement() {
		Agreement agreement = buildAgreement()
		agreement.addToTenats buildTenant()
		agreement.addToPeriods buildPeriod()
		safeSaveDomain agreement
	}

	private def constructWaterProviderInfo() {
		ProviderInfo providerInfo = new ProviderInfo(serviceType: ServiceType.WATER)
		safeSaveDomain providerInfo


		PayInfo pi = new PayInfo(payType: PayType.WATER_QUANTITY)
		safeSaveDomain pi

		providerInfo.addToPayInfo(pi)
		safeSaveDomain providerInfo
	}

	private def constructGasProviderInfo() {
		ProviderInfo providerInfo = buildProviderInfoGas()
		def payInfoQuantity = buildAndSafeGasInfoQuantity ()
		providerInfo.addToPayInfo(payInfoQuantity)
		safeSaveDomain providerInfo
	}


	private def constructCurrent1ProviderInfo() {
		ProviderInfo providerInfo = buildProviderInfoCurrent()
		def payInfoQuantity1 = buildAndSafeCurrent1InfoQuantity ()
		def payInfoQuantity2 = buildAndSafeCurrent2InfoQuantity ()
		providerInfo.addToPayInfo(payInfoQuantity1)
		providerInfo.addToPayInfo(payInfoQuantity2)
		safeSaveDomain providerInfo
	}

	private Period buildPeriod() {
		Period period = new Period()
		period.addToProviders buildGasProvider()
		period.addToProviders buildWaterProvider()
		period.addToProviders buildCurrent1Provider()
		safeSaveDomain period
	}

	private Provider buildGasProvider() {
		Provider waterProvider = new Provider(name: 'Gazowe przedsiebiorstwo ok')
		waterProvider.providerInfo = buildProviderInfoGas()
		waterProvider.address = buildAddress()
		waterProvider.addToFactors buildConstGasFactor()
		waterProvider.addToFactors buildVariableGasFactor()
		safeSaveDomain waterProvider
	}

	private Provider buildWaterProvider() {
		Provider waterProvider = new Provider(name: 'Wodne przedsiebiorstwo ok')
		ProviderInfo providerInfo = new ProviderInfo(serviceType: ServiceType.WATER)
		safeSaveDomain providerInfo
		waterProvider.providerInfo = providerInfo
		waterProvider.address = buildAddress()
		waterProvider.addToFactors buildConstWaterFactor()
		waterProvider.addToFactors buildVariableWaterFactor()
		safeSaveDomain waterProvider
	}
	//TODO: rahu this is ugly creation of the Provider and ProviderInfo again in real it will be taken from the Home relation
	private Provider buildCurrent1Provider() {
		Provider currentProvider = new Provider(name: 'Prad tauron')
		ProviderInfo providerInfo = new ProviderInfo(serviceType: ServiceType.CURRENT)
		safeSaveDomain providerInfo
		currentProvider.providerInfo = providerInfo
		currentProvider.address = buildAddress()
		currentProvider.addToFactors buildConstCurrent1Factor()
		currentProvider.addToFactors buildVariableCurrent1Factor()
		currentProvider.addToFactors buildConstCurrent2Factor()
		currentProvider.addToFactors buildVariableCurrent2Factor()
		safeSaveDomain currentProvider
	}

	private VariableFactor buildVariableGasFactor() {
		VariableFactor quantityFactor = new VariableFactor(price: 1.3, unit: 'zl', name: 'gas variable')
		quantityFactor.factorType = FactorType.Gas
		quantityFactor.payInfo = buildAndSafeGasInfoQuantity()
		safeSaveDomain quantityFactor
	}

	private VariableFactor buildVariableWaterFactor() {
		VariableFactor quantityFactor = new VariableFactor(price: 1.7, unit: 'zl', name: 'water variable')
		quantityFactor.factorType = FactorType.Water
		PayInfo pi = new PayInfo(payType: PayType.WATER_QUANTITY)
		safeSaveDomain pi
		quantityFactor.payInfo = pi
		safeSaveDomain quantityFactor
	}

	private VariableFactor buildVariableCurrent1Factor() {
		VariableFactor quantityFactor = new VariableFactor(price: 0.43, unit: 'zl', name: 'day variable')
		quantityFactor.factorType = FactorType.Current1
		PayInfo pi = new PayInfo(payType: PayType.CURRENT_DAY)
		safeSaveDomain pi
		quantityFactor.payInfo = pi
		safeSaveDomain quantityFactor
	}

	private VariableFactor buildVariableCurrent2Factor() {
		VariableFactor quantityFactor = new VariableFactor(price: 0.24, unit: 'zl', name: 'night variable')
		quantityFactor.factorType = FactorType.Current2
		PayInfo pi = new PayInfo(payType: PayType.CURRENT_NIGHT)
		safeSaveDomain pi
		quantityFactor.payInfo = pi
		safeSaveDomain quantityFactor
	}

	private ConstFactor buildConstGasFactor() {
		ConstFactor constFactor = new ConstFactor(price: 10, unit: 'zl', name: 'gas subscription')
		constFactor.factorType = FactorType.Gas
		safeSaveDomain constFactor
	}

	private ConstFactor buildConstWaterFactor() {
		ConstFactor constFactor = new ConstFactor(price: 12, unit: 'zl', name: 'water subscription')
		constFactor.factorType = FactorType.Water
		safeSaveDomain constFactor
	}

	private ConstFactor buildConstCurrent1Factor() {
		ConstFactor constFactor = new ConstFactor(price: 21, unit: 'zl', name: 'current subscription')
		constFactor.factorType = FactorType.Current1
		safeSaveDomain constFactor
	}

	private ConstFactor buildConstCurrent2Factor() {
		ConstFactor constFactor = new ConstFactor(price: 23, unit: 'zl', name: 'current subscription')
		constFactor.factorType = FactorType.Current2
		safeSaveDomain constFactor
	}



	private Address buildAddress() {
		Address address = new Address(city: 'Opole', street: 'Wodna', homeNumber:1, zipCode:'AB-123')
		safeSaveDomain address
	}

	private Agreement buildAgreement() {
		File scan = new File('agreement.txt')
		Agreement agreement = new Agreement(scanAgreement: scan)
		agreement
	}

	private Tenant buildTenant() {
		Tenant tenant = new Tenant(name: 'Katrina', furName: 'Torbicka', userName: 'torba')
		safeSaveDomain tenant
	}

	private ProviderInfo buildProviderInfoGas() {
		ProviderInfo providerInfo = new ProviderInfo(serviceType: ServiceType.GAS)
		safeSaveDomain providerInfo
	}

	private ProviderInfo buildProviderInfoCurrent() {
		ProviderInfo providerInfo = new ProviderInfo(serviceType: ServiceType.CURRENT)
		safeSaveDomain providerInfo
	}


	private Home buildHome() {
		Address address = new Address(city:'Opole',street: 'Wandy', homeNumber: 3, zipCode:'US-123')
		safeSaveDomain address
		new Home(address: address, name:'WANDY')
	}

	private def buildAndSafeGasInfoQuantity() {
		PayInfo pi = new PayInfo(payType: PayType.GAS_QUANTITY)
		safeSaveDomain pi
	}

	private def buildAndSafeCurrent1InfoQuantity() {
		PayInfo pi = new PayInfo(payType: PayType.CURRENT_DAY)
		safeSaveDomain pi
	}

	private def buildAndSafeCurrent2InfoQuantity() {
		PayInfo pi = new PayInfo(payType: PayType.CURRENT_NIGHT)
		safeSaveDomain pi
	}

	private def safeSaveDomain(def domainObject) {
		assert domainObject.validate()
		domainObject.save(failOnError: true)
		domainObject
	}


	def destroy = {
	}
}
