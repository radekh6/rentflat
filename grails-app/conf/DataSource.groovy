dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    dialect="org.hibernate.dialect.MySQL5InnoDBDialect"
    formatSql=true
}
/*dataSource {
    pooled = true
    jmxExport = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
}*/
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
//    cache.region.factory_class = 'org.hibernate.cache.SingletonEhCacheRegionFactory' // Hibernate 3
    cache.region.factory_class = 'org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory' // Hibernate 4
    singleSession = true // configure OSIV singleSession mode
    flush.mode = 'manual' // OSIV session flush mode outside of transactional context
}

// environment specific settings
environments {
    dbmigration {
        // extra environment only for running db migration plugin scripts
        // use environment by calling grails command with: -Dgrails.env=dbmigration
        dataSource {
            url = "jdbc:mysql://localhost/rentFlats_build?autoReconnect=true&useUnicode=true&characterEncoding=utf-8"
            username = "rentFlats"
            password = "rentUser1"
        }
    }
    development {
        dataSource {
            dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:mysql://localhost/rentFlats?autoReconnect=true&useUnicode=true&characterEncoding=utf-8"
            username = "rentFlats"
            password = "rentUser1"
            //url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
        }
    }
    test {
        dataSource {
            dbCreate = "create"
            url = "jdbc:mysql://localhost/rentFlatsTest?autoReconnect=true&useUnicode=true&characterEncoding=utf-8"
            username = "rentFlats"
            password = "rentUser1"
        }
    }
    production {
        dataSource {
            dbCreate = "validate"
            jndiName = "java:comp/env/rentFlatsDb"
        }
    }
}
