package eu.testowanie

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap

class WebhookController {

    private final static HUB_MODE = "hub.mode"
    private final static HUB_VERIFY_TOKEN = "hub.verify_token"
    private final static token = 'ThisIsMyVerifyTokenSetInFacebookSettingPsnel'
    private final static subscribe = 'subscribe'
    private final static HUB_CHALLENGE = "hub.challenge"

    static allowedMethods = [verifyHookSubsciption: ["POST", "GET"]]

    def verifyHookSubsciption() {
        if(request.get) {
            renderSuccessVerificationToFacebook()
            render(status: 400 /*org.apache.http.HttpStatus.SC_BAD_REQUEST*/)
        }
        if(request.post) {
            processFacebookHook()
            render (status: 200 /*org.apache.http.HttpStatus.SC_OK*/)
        }
    }

    private def processFacebookHook() {

        GrailsParameterMap myData = params
        if (validateSigantureCall()) {
            def jsonResult = request.JSON
        }
    }

    private boolean validateSigantureCall() {
        def result = false
        request.headerNames.each {
            if (it == 'X-Hub-Signature') {
                result = validateHookRequest()
            }
        }
        result
    }

    private boolean validateHookRequest() {
        true
    }

    private void renderSuccessVerificationToFacebook() {
        String hubMode = params."${HUB_MODE}"
        String verifyToken = params."${HUB_VERIFY_TOKEN}"
        if (hubMode == subscribe && verifyToken == token) {
            render(text: params."${HUB_CHALLENGE}")
        }
    }
}
