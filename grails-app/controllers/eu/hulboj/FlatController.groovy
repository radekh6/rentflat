package eu.hulboj



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FlatController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def paramService

    def welcome() {
        flash.message = "fffff"
        paramService.setMenuSelector(ParamService.MenuSelector.start)
        render (view: "welcome", model: [flaga: false])
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Flat.list(params), model:[flatInstanceCount: Flat.count()]
    }

    def show(Home flatInstance) {
        respond flatInstance
    }

    def create() {
        respond new Home(params)
    }

    @Transactional
    def save(Home flatInstance) {
        if (flatInstance == null) {
            notFound()
            return
        }

        if (flatInstance.hasErrors()) {
            respond flatInstance.errors, view:'create'
            return
        }

        flatInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'flat.label', default: 'Flat'), flatInstance.id])
                redirect flatInstance
            }
            '*' { respond flatInstance, [status: CREATED] }
        }
    }

    def edit(Home flatInstance) {
        respond flatInstance
    }

    @Transactional
    def update(Home flatInstance) {
        if (flatInstance == null) {
            notFound()
            return
        }

        if (flatInstance.hasErrors()) {
            respond flatInstance.errors, view:'edit'
            return
        }

        flatInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Flat.label', default: 'Flat'), flatInstance.id])
                redirect flatInstance
            }
            '*'{ respond flatInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Home flatInstance) {

        if (flatInstance == null) {
            notFound()
            return
        }

        flatInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Flat.label', default: 'Flat'), flatInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'flat.label', default: 'Flat'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
