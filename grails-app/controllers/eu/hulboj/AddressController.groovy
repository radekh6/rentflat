package eu.hulboj



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AddressController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Address.list(params), model:[flatAddressInstanceCount: Address.count()]
    }

    def show(Address flatAddressInstance) {
        respond flatAddressInstance
    }

    def create() {
        respond new Address(params)
    }

    @Transactional
    def save(Address flatAddressInstance) {
        if (flatAddressInstance == null) {
            notFound()
            return
        }

        if (flatAddressInstance.hasErrors()) {
            respond flatAddressInstance.errors, view:'create'
            return
        }

        flatAddressInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'flatAddress.label', default: 'FlatAddress'), flatAddressInstance.id])
                redirect flatAddressInstance
            }
            '*' { respond flatAddressInstance, [status: CREATED] }
        }
    }

    def edit(Address flatAddressInstance) {
        respond flatAddressInstance
    }

    @Transactional
    def update(Address flatAddressInstance) {
        if (flatAddressInstance == null) {
            notFound()
            return
        }

        if (flatAddressInstance.hasErrors()) {
            respond flatAddressInstance.errors, view:'edit'
            return
        }

        flatAddressInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'FlatAddress.label', default: 'FlatAddress'), flatAddressInstance.id])
                redirect flatAddressInstance
            }
            '*'{ respond flatAddressInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Address flatAddressInstance) {

        if (flatAddressInstance == null) {
            notFound()
            return
        }

        flatAddressInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'FlatAddress.label', default: 'FlatAddress'), flatAddressInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'flatAddress.label', default: 'FlatAddress'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    private 'this is my great gift'() {

    }
}
