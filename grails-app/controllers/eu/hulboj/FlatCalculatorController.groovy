package eu.hulboj

import eu.hulboj.form.Amount

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FlatCalculatorController  {

    FlatCalculator flatCalculator
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def paramService

    /*This will be request from the client*/
    def flatList() {
      def billList = []
      render(template: "flatTemplates/factorTable", model: [billList: billList])
    }

    def flat() {
        paramService.setMenuSelector(ParamService.MenuSelector.flat)
        def billList = Amount.findAll()
        render(view: "flat", model: [flaga: false, billList: billList])
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond FlatCalculator.list(params), model:[flatCalculatorInstanceCount: FlatCalculator.count()]
    }

    def show(FlatCalculator flatCalculatorInstance) {
        respond flatCalculatorInstance
    }

    def create() {
        respond new FlatCalculator(params)
    }

    @Transactional
    def save(FlatCalculator flatCalculatorInstance) {
        if (flatCalculatorInstance == null) {
            notFound()
            return
        }

        if (flatCalculatorInstance.hasErrors()) {
            respond flatCalculatorInstance.errors, view:'create'
            return
        }

        flatCalculatorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'flatCalculator.label', default: 'FlatCalculator'), flatCalculatorInstance.id])
                redirect flatCalculatorInstance
            }
            '*' { respond flatCalculatorInstance, [status: CREATED] }
        }
    }

    def edit(FlatCalculator flatCalculatorInstance) {
        respond flatCalculatorInstance
    }

    @Transactional
    def update(FlatCalculator flatCalculatorInstance) {
        if (flatCalculatorInstance == null) {
            notFound()
            return
        }

        if (flatCalculatorInstance.hasErrors()) {
            respond flatCalculatorInstance.errors, view:'edit'
            return
        }

        flatCalculatorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'FlatCalculator.label', default: 'FlatCalculator'), flatCalculatorInstance.id])
                redirect flatCalculatorInstance
            }
            '*'{ respond flatCalculatorInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(FlatCalculator flatCalculatorInstance) {

        if (flatCalculatorInstance == null) {
            notFound()
            return
        }

        flatCalculatorInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'FlatCalculator.label', default: 'FlatCalculator'), flatCalculatorInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'flatCalculator.label', default: 'FlatCalculator'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
