package eu.hulboj

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.springframework.web.multipart.commons.CommonsMultipartFile


class AgreementController {

    AgreementService agreementService

    static String PATTERN = "MM/dd/yyyy"

    def save() {

        DateTime startDate = getStartDate(params.startDate_value)
        DateTime endDate = getEndDate(params.endDate_value)
        File file = getFile(params.file)

        try {
            agreementService.saveAgreement(startDate, endDate, file)
        } catch (Exception ex) {
            String errorMessage = ex.toString()
            flash.serverError = errorMessage
            log.error(errorMessage)
        }
        redirect(controller: "configurator", action: "configure")
    }

    File getFile(CommonsMultipartFile multipartFile) {
        File file = null
        try {
            file = new File(multipartFile.getOriginalFilename())
            multipartFile.transferTo(file)

        } catch (Exception ex) {
            log.info("No file selected" + ex.toString())
        }
        file
    }

    DateTime getEndDate(String endDate) {
        DateTime.parse(endDate, DateTimeFormat.forPattern(PATTERN))
    }

    DateTime getStartDate(String startDate) {
        DateTime.parse(startDate, DateTimeFormat.forPattern(PATTERN))
    }
}
