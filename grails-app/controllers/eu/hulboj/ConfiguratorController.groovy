package eu.hulboj

class ConfiguratorController {

	def paramService
	
    def index() { }
	
	def configure() {
		def status = params.serverStatus
		paramService.setMenuSelector(ParamService.MenuSelector.configurator)
		render(view: "flat", model: [flaga: false, status: status])
	}
}
