package eu.hulboj

import eu.hulboj.form.Amount
import eu.hulboj.form.Record
import rentflats.BillService
import rentflats.calculator.ChargerCalculatorService
import rentflats.calculator.ChargerFormatCalculatorService
import rentflats.database.BillDatabaseService
import org.springframework.transaction.annotation.Transactional
import rentflats.json.JsonAmountService
import rentflats.json.JsonRecordService

@Transactional
class BillController {

    BillDatabaseService billDatabaseService
    JsonAmountService jsonAmountService
    JsonRecordService jsonRecordService
    ChargerCalculatorService chargerCalculatorService
    ChargerFormatCalculatorService chargerFormatCalculatorService
    BillService billService

    def createBill() {

        long homeId = mockHome()

        def billJson = request.JSON
        if (jsonAmountService.hasValidBillJson(billJson)) {
            Amount amount = jsonAmountService.buildAmountFromJson billJson
            List<Record> records = jsonRecordService.buildRecordsFromJson(billJson)
            billService.addRecordsTo amount, records
            amount.calculatedCharge = chargerCalculatorService.calculateChargeForBill(amount, homeId)
            amount.calculatedChargeFormat = chargerFormatCalculatorService.calculateChargeForBill(amount, homeId)
            if (billDatabaseService.saveBill(amount)) {
                def billList = billDatabaseService.billsByMonths()
                log.info billList.toString()
                render(template: "/flatCalculator/flatTemplates/factorTable", model: [billList: billList, payment: amount.calculatedChargeFormat])
            } else {
                log.error "bill not saved!!!"
                render(status: 500, text: message(code: "server.response.fail", default: 'fail'))
            }
        } else {
            log.error "json is not valid:" + billJson.toString()
            render(status: 401, text: message(code: "server.response.fail", default: 'fail json'))
        }

    }

    private long mockHome() {
        Home home = Home.list().first()
        Long homeId = home.id
        homeId
    }
}