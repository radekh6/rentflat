package eu.hulboj

import grails.transaction.Transactional

@Transactional
class ParamService {

    enum MenuSelector {
        start, flat, configurator, contact
    }

    MenuSelector currentSelector = MenuSelector.start

    def setMenuSelector(MenuSelector current) {
        currentSelector = current
    }
}
