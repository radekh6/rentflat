package eu.hulboj

/**
 * Created by radoslaw on 11.01.16.
 */
class SandboxService {
    /**
     * TDD for parsing text for styling for HTML protocol
     * @param pureText
     * @return
     */
    String decorateHtmlWithSpan(def pureText) {
        String newLineList = pureText.replace("\n", " </br>")
        List<String> list = newLineList.split("</br>")
        def chunk = ""
        list.each {
            if (it != " ") {
                chunk += "<span>" + it + "</span>"
            }
            if(list.size() > 1) chunk += "</br>"
            if(list.size() == 1 && newLineList.contains("</br>")) chunk += "</br>"
        }
        chunk.trim()
    }
}
