package eu.hulboj

import eu.hulboj.Calculator.Agreement
import grails.transaction.Transactional
import org.joda.time.DateTime

@Transactional
class AgreementService {

	/**
	 * 
	 * @param endDate
	 * @return new Agreement
	 */
    Agreement createAgreement(DateTime endDate) {
		createAgreement(new DateTime(), endDate)
    }

	Agreement createAgreement(DateTime startDate, DateTime endDate) {
		new Agreement(startDate: startDate, endDate: endDate)
	}
	
	/**
	 * 
	 * @param pTenant the Tenant who looking for his agreement
	 * @return Agreement which belongs to pTenant
	 */
	Agreement getAgreement(Tenant pTenant) {
		Agreement retVal = null
		Agreement.findAll().each {Agreement agr ->
			agr.tenats.each {Tenant tenant ->
				if(tenant == pTenant)
					return retVal = agr

			}
		}
		retVal
	}
	
	/**
	 * 
	 * @param flat
	 * @return The current(actual) agreement of the flat
	 */
	Agreement getActiveAgreement(Home pFlat) {
		getAgreement(pFlat, new Date())
	}
	
	/**
	 * 
	 * @param date
	 * @return The agreement of the flat active for date parameter
	 */
	Agreement getAgreement(Home pFlat, Date date) {
		Agreement retVal
		pFlat.agreements.each {
			if(date.before(it.endDate) && date.after(it.startDate)) {
				retVal = it
			}
	   	}
		retVal
	}

	Boolean saveAgreement(DateTime startDate, DateTime endDate, File fileAgreement) {
		Agreement agreement = createAgreement(startDate, endDate)
		agreement.scanAgreement = fileAgreement
 		agreement.save(failOnError: true)
	}
}
