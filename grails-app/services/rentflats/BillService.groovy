package rentflats

import eu.hulboj.form.Amount
import eu.hulboj.form.Record
import grails.transaction.Transactional

@Transactional
class BillService {

    void addRecordsTo(Amount amount, List<Record> records) {
        records.each { record ->
            amount.addToRecords(record)
        }
    }
}
