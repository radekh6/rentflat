package rentflats.database

import eu.hulboj.Calculator.Period
import eu.hulboj.Calculator.Provider
import eu.hulboj.config.ProviderInfo
import grails.transaction.Transactional

@Transactional
class PeriodDatabaseService {

    Provider getProviderFor(Period period, ProviderInfo providerInfo) {
        period.providers.find {it.providerInfo == providerInfo}
    }
}
