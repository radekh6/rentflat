package rentflats.database

import eu.hulboj.Calculator.ConstFactor
import eu.hulboj.Calculator.Period
import eu.hulboj.Calculator.Provider
import eu.hulboj.Calculator.VariableFactor
import eu.hulboj.Home
import eu.hulboj.config.ProviderInfo
import eu.hulboj.form.Record
import grails.transaction.Transactional

@Transactional
class FactorDatabaseService {

    HomeDatabaseService homeDatabaseService
    AgreementDatabaseService agreementDatabaseService
    PeriodDatabaseService periodDatabaseService

    VariableFactor getVariableFactor(Record record, long homeId) {
        Provider provider = getProviderInfo(homeId, record.providerInfo)
        ProviderDatabaseService.getVariableFactorFor (record.payInfo, provider)
    }

    ConstFactor getConstFactor(ProviderInfo providerInfo, long homeId) {
        Provider provider = getProviderInfo(homeId, providerInfo)
        ProviderDatabaseService.getConstFactorsFor provider
    }

    private Provider getProviderInfo(long homeId, ProviderInfo providerInfo) {
        Home home = homeDatabaseService.getHomeBy(homeId)
        Period period = agreementDatabaseService.getLastPeriodFor(home)
        periodDatabaseService.getProviderFor(period, providerInfo)
    }

}
