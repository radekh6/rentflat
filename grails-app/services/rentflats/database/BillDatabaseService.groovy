package rentflats.database

import eu.hulboj.form.Amount
import grails.transaction.Transactional

@Transactional
class BillDatabaseService {

    boolean saveBill(Amount bill) {
        try {
            assert bill.validate()
            bill.save(failOnError: true)
        } catch (Exception e) {
            log.error e.stackTrace.toString()
        }
    }

    List<Map> billsByMonths() {
        def query =
        $/
        SELECT new map (b.id as id, b.billMonth as billMonth, b.water as water, b.gas as gas, b.current1 as current1, b.current2 as current2, time(b.billMonth) as time)
               FROM Amount b
               WHERE b.id IN (
                    SELECT MAX(bn.id)
                    FROM Amount bn
                    GROUP BY  day(bn.billMonth),
                              month(bn.billMonth),
                              year(bn.billMonth) )
        /$
        Amount.executeQuery query
    }

    // TODO: rahu may be this solve problem if times are the same for differ date
    /*SELECT a.*  FROM messages a
    INNER JOIN
    (SELECT name, max(other_col) as other_col
    FROM messages GROUP BY name) as b ON
    a.name = b.name
    AND a.other_col = b.other_col*/

}
