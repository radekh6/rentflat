package rentflats.database

import eu.hulboj.Calculator.Agreement
import eu.hulboj.Calculator.Period
import eu.hulboj.Home
import grails.transaction.Transactional

@Transactional
class AgreementDatabaseService {

    Period getLastPeriodFor(Home home) {
        Agreement agreement = home?.agreements.last()
        agreement?.periods.last()
    }
}
