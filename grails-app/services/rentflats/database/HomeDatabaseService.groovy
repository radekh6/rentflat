package rentflats.database

import eu.hulboj.Home
import eu.hulboj.config.PayInfo
import eu.hulboj.config.PayType
import eu.hulboj.config.ProviderInfo
import eu.hulboj.config.ServiceType
import grails.transaction.Transactional


@Transactional
class HomeDatabaseService {

    def getProviderInfo(PayType payType, ServiceType serviceType) {
        PayInfo pi  = PayInfo.findByPayType payType
        ProviderInfo providerInfo = ProviderInfo.findByServiceType serviceType
        providerInfo.addToPayInfo(pi)
        providerInfo
    }

    def getPayInfo(PayType payType) {
        PayInfo.findByPayType payType
    }

    Home getHomeBy(long homeId) {
        Home.findById homeId
    }
}
