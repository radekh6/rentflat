package rentflats.database

import eu.hulboj.Calculator.ConstFactor
import eu.hulboj.Calculator.Provider
import eu.hulboj.Calculator.VariableFactor
import eu.hulboj.config.PayInfo
import grails.transaction.Transactional

@Transactional
class ProviderDatabaseService {

    static VariableFactor getVariableFactorFor(PayInfo payInfo, Provider provider) {
        provider.factors.find {
            (it.class == VariableFactor) && (it as VariableFactor).payInfo == payInfo
        } as VariableFactor
    }

    static ConstFactor getConstFactorsFor(Provider provider) {
        provider.factors.find {
            it.class == ConstFactor
        }
    }
}
