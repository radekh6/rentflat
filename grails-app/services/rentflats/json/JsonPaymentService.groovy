package rentflats.json

import grails.transaction.Transactional

@Transactional
class JsonPaymentService {


    boolean hasValidPaymentJson(def json) {
        try {
            if (json instanceof Map &&
                    (json.current1 as Integer).class == Integer &&
                    (json.current2 as Integer).class == Integer &&
                    (json.gas as Integer).class == Integer &&
                    (json.water as Integer).class == Integer) {
                return true
            }
            return false
        } catch (NumberFormatException nfe) {
            return false
        }
    }

}
