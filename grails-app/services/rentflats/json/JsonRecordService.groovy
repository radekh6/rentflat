package rentflats.json

import eu.hulboj.form.Record
import grails.transaction.Transactional
import rentflats.builders.FactoryRecordBuilder

@Transactional
class JsonRecordService {

    FactoryRecordBuilder factoryRecordBuilder

    List<Record> buildRecordsFromJson(def json) {
        List<Map<Map, BuildRecord>> commandRecordMap = factoryRecordBuilder.build(json)
        commandRecordMap.collect { item ->
            item.buildRecord.buildRecordFrom item.map
        }
    }
}
