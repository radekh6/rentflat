package rentflats.json

import eu.hulboj.form.Amount
import grails.transaction.Transactional
import org.joda.time.DateTime

@Transactional
class JsonAmountService {

    boolean hasValidBillJson(def factors) {
        if (factors instanceof Map)
            if ((factors?.water as Integer).class == Integer &&
                    (factors?.gas as Integer).class == Integer &&
                    (factors?.current1 as Integer).class == Integer) {
                if (factors?.current2 != null) {
                    if((factors?.current2 as String) == '') {
                        return  true
                    } else if ((factors?.current2 as Integer).class == Integer)
                        return true
                    else return false
                }
                return true
            }
        false
    }

    Amount buildAmountFromJson(factors) {
        Amount newBill = new Amount()
        newBill.billMonth = new DateTime()
        newBill.water = factors.water as Integer
        newBill.gas = factors.gas as Integer
        newBill.current1 = factors.current1 as Integer
        if (factors?.current2 != '')
            newBill.current2 = factors?.current2 as Integer
        newBill
    }
}
