package rentflats.calculator

import eu.hulboj.Payment
import eu.hulboj.config.ProviderInfo
import eu.hulboj.form.Amount
import eu.hulboj.form.Record
import grails.transaction.Transactional
import rentflats.database.FactorDatabaseService

@Transactional
class ChargerFormatCalculatorService {

    ChargerCalculatorService chargerCalculatorService
    FactorDatabaseService factorDatabaseService

    String calculateChargeForBill(Amount amount, long homeId) {
        def String sumResult = amount.records.sum { Record record ->
            def variableFactor = forLastPeriodGetVariableFactorFrom record, homeId
            def constFactor = forLastPeriodGetConstFactorFrom record.providerInfo, homeId
            "${constFactor.name}:${constFactor.price} + ( ${variableFactor.name}: ${variableFactor.price} * ${record.payInfo.payType}: ${record.value} ),"
        }
        sumResult + "Resume= ${sum(amount, homeId)}zl"
    }

    protected def forLastPeriodGetVariableFactorFrom (Record record, long homeId) {
        factorDatabaseService.getVariableFactor (record, homeId)
    }

    protected def forLastPeriodGetConstFactorFrom(ProviderInfo providerInfo, long homeId) {
        factorDatabaseService.getConstFactor (providerInfo, homeId)

    }

    private def sum (Amount amount, long homeId) {
        chargerCalculatorService.calculateChargeForBill(amount, homeId)
    }
}
