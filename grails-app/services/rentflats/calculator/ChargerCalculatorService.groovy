package rentflats.calculator

import eu.hulboj.config.ProviderInfo
import eu.hulboj.form.Amount
import eu.hulboj.form.Record
import grails.transaction.Transactional
import rentflats.database.FactorDatabaseService

@Transactional
class ChargerCalculatorService {

    FactorDatabaseService factorDatabaseService

    Double calculateChargeForBill(Amount amount, long homeId) {
        def Double sumResult = amount.records.sum { Record record ->
            def variableFactor = forLastPeriodGetVariableFactorFrom record, homeId
            def constFactor = forLastPeriodGetConstFactorFrom record.providerInfo, homeId
            constFactor.price + variableFactor.price * record.value
        }
        sumResult
    }

    protected def forLastPeriodGetVariableFactorFrom (Record record, long homeId) {
        factorDatabaseService.getVariableFactor (record, homeId)
    }

    protected def forLastPeriodGetConstFactorFrom(ProviderInfo providerInfo, long homeId) {
        factorDatabaseService.getConstFactor (providerInfo, homeId)

    }
}
