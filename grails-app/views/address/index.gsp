
<%@ page import="eu.hulboj.Address" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'flatAddress.label', default: 'FlatAddress')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-flatAddress" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-flatAddress" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="city" title="${message(code: 'flatAddress.city.label', default: 'City')}" />
					
						<th><g:message code="flatAddress.flat.label" default="Flat" /></th>
					
						<g:sortableColumn property="homeNumber" title="${message(code: 'flatAddress.homeNumber.label', default: 'Home Number')}" />
					
						<g:sortableColumn property="street" title="${message(code: 'flatAddress.street.label', default: 'Street')}" />
					
						<g:sortableColumn property="zipCode" title="${message(code: 'flatAddress.zipCode.label', default: 'Zip Code')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${flatAddressInstanceList}" status="i" var="flatAddressInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${flatAddressInstance.id}">${fieldValue(bean: flatAddressInstance, field: "city")}</g:link></td>
					
						<td>${fieldValue(bean: flatAddressInstance, field: "flat")}</td>
					
						<td>${fieldValue(bean: flatAddressInstance, field: "homeNumber")}</td>
					
						<td>${fieldValue(bean: flatAddressInstance, field: "street")}</td>
					
						<td>${fieldValue(bean: flatAddressInstance, field: "zipCode")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${flatAddressInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
