
<%@ page import="eu.hulboj.Address" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'flatAddress.label', default: 'FlatAddress')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-flatAddress" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-flatAddress" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list flatAddress">
			
				<g:if test="${flatAddressInstance?.city}">
				<li class="fieldcontain">
					<span id="city-label" class="property-label"><g:message code="flatAddress.city.label" default="City" /></span>
					
						<span class="property-value" aria-labelledby="city-label"><g:fieldValue bean="${flatAddressInstance}" field="city"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${flatAddressInstance?.flat}">
				<li class="fieldcontain">
					<span id="flat-label" class="property-label"><g:message code="flatAddress.flat.label" default="Flat" /></span>
					
						<span class="property-value" aria-labelledby="flat-label"><g:link controller="flat" action="show" id="${flatAddressInstance?.flat?.id}">${flatAddressInstance?.flat?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${flatAddressInstance?.homeNumber}">
				<li class="fieldcontain">
					<span id="homeNumber-label" class="property-label"><g:message code="flatAddress.homeNumber.label" default="Home Number" /></span>
					
						<span class="property-value" aria-labelledby="homeNumber-label"><g:fieldValue bean="${flatAddressInstance}" field="homeNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${flatAddressInstance?.street}">
				<li class="fieldcontain">
					<span id="street-label" class="property-label"><g:message code="flatAddress.street.label" default="Street" /></span>
					
						<span class="property-value" aria-labelledby="street-label"><g:fieldValue bean="${flatAddressInstance}" field="street"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${flatAddressInstance?.zipCode}">
				<li class="fieldcontain">
					<span id="zipCode-label" class="property-label"><g:message code="flatAddress.zipCode.label" default="Zip Code" /></span>
					
						<span class="property-value" aria-labelledby="zipCode-label"><g:fieldValue bean="${flatAddressInstance}" field="zipCode"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:flatAddressInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${flatAddressInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
