<%--
  Created by IntelliJ IDEA.
  User: hulborad
  Date: 2015-09-15
  Time: 12:01
--%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">


    <title><g:message code="page.layout.welcome.hallo" default="System rejestracji liczników"/></title>

    <g:javascript library="jquery" plugin="jquery"/>



    <asset:javascript src="/jquery/dist/jquery.js"/>
    <asset:javascript src="/bootstrap/js/bootstrap.js"/>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="${resource(dir:'/lib/bootstrap/dist/css',file:'bootstrap.min.css')}" />
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="${resource(dir:'/lib/bootstrap/dist/css/custom',file:'justified-nav.css')}" />
    <!-- Custom -->
    <link rel="stylesheet" href="${resource(dir:'/css',file:'mainFlat.css')}" />

    <asset:javascript src="/custom/mainScriptFlat.js"/>


    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <g:layoutHead/>
</head>

<body>

<div class="container">

    <!-- The justified navigation menu is meant for single line per list item.
           Multiple lines will require custom code not provided by Bootstrap. -->
    <div class="masthead">
        <h3 style="background-color: aliceblue" class="text-muted">
            <span>
                <img src="${resource(dir: 'custom', file: 'logoRentalFlats.png')}" style="-moz-transform:scale(0.5);-webkit-transform:scale(0.5);transform:scale(0.5);"/>
            </span>
            <span class="col-md-9>">
                <label style="color: #0f0f0f">System rejestracji liczników mieszkaniowych</label>
            </span>
        </h3>
        <nav>
            <ul class="nav nav-justified">
                %{--http://stackoverflow.com/questions/1829318/how-do-i-pass-a-variable-into-grails-template-from-a-layout-file--}%

                <g:set var="valStart" value="${message(code:'menu.item.Start', default: 'Start')}"/>
                <g:set var="valFlat" value="${message(code: 'menu.item.Flats', default: 'Flats')}"/>
                <g:set var="valConfigurator" value="${message(code: 'menu.item.Configurator', default: 'Configurator')}"/>
                <g:set var="valContact" value="${message(code: 'menu.item.Contact', default: 'Contact')}"/>
                <g:menuSelector selector="${eu.hulboj.ParamService.MenuSelector.start}" value="${valStart}" controller="flat" action="welcome"/>
                <g:menuSelector selector="${eu.hulboj.ParamService.MenuSelector.flat}" value="${valFlat}" controller="flatCalculator" action="flat"/>
                <g:menuSelector selector="${eu.hulboj.ParamService.MenuSelector.configurator}" value="${valConfigurator}" controller="configurator" action="configure"/>
                <g:menuSelector selector="${eu.hulboj.ParamService.MenuSelector.contact}" value="${valContact}" controller="flat" action="welcome"/>

            </ul>
        </nav>
    </div>
    <div id="wrap">
        <g:if test="${true}">
            <div id="flash-message" class="modal fade">
                <div class="alert alert-error">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <%-- do not use ${} with default HTML encoding as we have flash messages formatted with <br> tags --%>
                    <%=flash.message%>
                </div>
            </div>
        </g:if>
    </div>
   <g:layoutBody/>
   
   <!-- Example row of columns -->
    <div class="row">
        <div class="col-lg-4">
            <h2>Wiadomości!</h2>
            <p class="text-danger">Każdego 1 dnia miesiąca proszę o akutalizacje liczników</p>
            <p>Liczniki są aktualizowane od 1-3 dnia każdego miesiąca. Po zatwierdzeniu nie można aktualizować liczników. Opłaty miesięczne są do 15 dnia miesiąca.</p>
            <p><a class="btn btn-primary" href="#" role="button">Szczegóły &raquo;</a></p>
        </div>
        <div class="col-lg-4">
            <h2>Ogólne warunki użytkowania</h2>
            <p>Zmiany: malowanie, umeblowanie itp</p>
            <p><a class="btn btn-primary" href="#" role="button">Szczegóły &raquo;</a></p>
        </div>
        <div class="col-lg-4">
            <h2>Galeria</h2>
            <p>Zdjecia mieszkania, przed wynajeciem.</p>
            <p><a class="btn btn-primary" href="#" role="button">Szczegóły &raquo;</a></p>
        </div>
    </div>

    <!-- Site footer -->
    <footer class="footer">
        <p>&copy; RH Company 2015</p>
    </footer>

</div> <!-- /container -->


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>
