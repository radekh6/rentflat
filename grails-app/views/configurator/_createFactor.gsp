<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-3">
                <span class="label label-default">Create</span>
                <div class="row-lg-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Name:</button>
                        </span>
                        <input type="text" class="form-control" placeholder="name...">
                        <!--http://www.eyecon.ro/bootstrap-datepicker/# -->
                    </div>
                </div>
                <g:spaceBr count="1"/>
                <span class="label label-default">Create</span>
                <div class="row-lg-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Unit:</button>
                        </span>
                        <input type="text" class="form-control" placeholder="unit...">
                    </div>
                </div>
                <g:spaceBr count="1"/>
                <span class="label label-default">Create</span>
                <div class="row-lg-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Price:</button>
                        </span>
                        <input type="text" class="form-control" placeholder="price...">
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <g:spaceBr count="1" />
                <div class="row-lg-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Add</button>
                            <button class="btn btn-default" type="button">Save</button>
                        </span>
                        <button class="btn btn-default" type="button">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>