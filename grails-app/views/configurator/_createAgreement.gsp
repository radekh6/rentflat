<g:uploadForm name="postAgreementForm" controller="agreement" action="save" onsubmit="disableMediaFileUpload();" class="form-horizontal" data-target="#postStatus">
<g:hiddenField name="link" value="${postMsgInstance?.link}"/>
<fieldset>
    <g:if test="${params.serverStatus == "500"}">
        <g:if test="${flash.serverError}">
            <div class="alert alert-error" style="display: block">${flash.serverError}</div>
        </g:if>
    </g:if>
    <g:else>
        <div class="panel-body">
            <calendar:resources lang="en" theme="tiger"/>
            <div class="col-xs-6">
                <div class="form-group" >
                    <label for="startDate" class="control-label col-xs-3">Start date:</label>
                    <div class="col-xs-9">
                        <calendar:datePicker style="height: 100%" class="form-control" name="startDate"
                                             years="${2014..2017}" value="${new Date()}"
                                             defaultValue="${new Date()}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="endDate" class="control-label col-xs-3">End date:</label>
                    <div class="col-xs-9">
                        <calendar:datePicker style="width: 100%; height: 100%" class="form-control" name="endDate"
                                             years="${2014..2017}" value="${new Date()}"
                                             defaultValue="${new Date()}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="file" class="control-label col-xs-3">File:</label>
                    <div class="col-xs-9" >
                        <input style="height: 0%" class="form-control" type='file' size="22" value="" default="ddd" name='file'/>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </g:else>


</fieldset>
</g:uploadForm>