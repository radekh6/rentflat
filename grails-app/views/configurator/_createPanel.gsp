<div id="tabPanelDiv">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Agreement</a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Provider</a></li>
        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Factor</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home"><g:render template="createAgreement"/></div>
        <div role="tabpanel" class="tab-pane" id="profile"><g:render template="createProvider"/></div>
        <div role="tabpanel" class="tab-pane" id="messages"><g:render template="createFactor"/></div>
    </div>
    <div id="postStatus"/>
</div>