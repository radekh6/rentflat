<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="layout" content="welcomedec">
<title>Insert title here</title>
</head>
<body>
  <div class="body">
    <h2>Configurator</h2>
    <p id="selectedTown" class="lead">Choose the town to create agreement.</p>
    <g:render template="/template/dropdown"/>
    <g:spaceBr count="1"/>
    <g:render template="createPanel"/>
    <g:spaceBr count="3"/>
  </div>
</body>
</html>