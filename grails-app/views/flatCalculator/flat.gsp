<%--
  Created by IntelliJ IDEA.
  User: hulborad
  Date: 2015-09-15
  Time: 12:01
--%>

<%@ page import="eu.hulboj.FlatCalculator" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="welcomedec">

    <asset:javascript src="/flat/collectorBill.js"/>
    <asset:javascript src="/flat/clientBill.js"/>

</head>

<body>
<parameter name="selector" value="flat"/>

<g:render template="flatTemplates/insertBill"/>
<g:render template="flatTemplates/factorTable" model="[billList: billList]"/>

</body>
</html>
