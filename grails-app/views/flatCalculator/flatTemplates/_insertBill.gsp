<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-3">
                <span class="label label-default">Edit</span>
                <div class="row-lg-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Water:</button>
                        </span>
                        <input id="water" type="number" class="form-control" placeholder="value...">
                        <!--http://www.eyecon.ro/bootstrap-datepicker/# -->
                    </div>
                </div>
                <g:spaceBr count="1"/>
                <span class="label label-default">Edit</span>
                <div class="row-lg-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Gas:</button>
                        </span>
                        <input id="gas" type="number" class="form-control" placeholder="value...">
                    </div>
                </div>
                <g:spaceBr count="1"/>
                <span class="label label-default">Edit</span>
                <div class="row-lg-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Current 1:</button>
                        </span>
                        <input id="current1" type="number" class="form-control" placeholder="value...">
                    </div>
                </div>
                <g:spaceBr count="1"/>
                <span class="label label-default">Edit</span>
                <div class="row-lg-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Current 2:</button>
                        </span>
                        <input id="current2" type="number" class="form-control" placeholder="value...">
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <g:spaceBr count="1" />
                <div class="row-lg-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button"
                                    onclick="moduleBill.createBill('<g:createLink controller="bill" action="createBill" />', {
                                        removeTable:'tableFactorsReport',
                                        updatedSelector:'tableFactorsReportContainer'
                                    })">Submit</button>
                            <button class="btn btn-default" type="button">Cancel</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>