<%@ page import="grails.converters.JSON" %>
<div id="tableFactorsReportContainer">
    <div id="tableFactorsReport">
        <div class="row-fluid span1">&nbsp;</div>
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <g:sortableColumn property="name"
                                  title="${message(code: 'view.flat.table.month', default: 'Month')}"/>
                <g:sortableColumn property="name"
                                  title="${message(code: 'view.flat.table.water', default: 'Water')}"/>
                <g:sortableColumn property="name"
                                  title="${message(code: 'view.flat.table.gas', default: 'Gas')}"/>
                <g:sortableColumn property="name"
                                  title="${message(code: 'view.flat.table.current' + '(1)', default: 'Electricity(1)')}"/>
                <g:sortableColumn property="name"
                                  title="${message(code: 'view.flat.table.current' + '(2)', default: 'Electricity(2)')}"/>
                <g:sortableColumn property="name"
                                  title="${message(code: 'view.flat.table.resume' + ' 2', default: 'Resume')}"/>
            </tr>
            </thead>
            <tbody>
            <g:each in="${billList}" status="i" var="billItem">
                <tr id="${billItem.billMonth}">
                    <g:hiddenField name="reportRowId" value="${billItem.id}"/>
                    <td>
                    <g:formatDate format="MMMM yyyy" date="${billItem.billMonth.toDate()}"/>

                    </td>
                    <td>
                        ${billItem.water}
                    </td>
                    <td>
                        ${billItem.gas}
                    </td>
                    <td>
                        ${billItem.current1}
                    </td>
                    <td>
                        ${billItem.current2}
                    </td>
                    <td>
                       <g:renderToHtml calculator="${payment}"/>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>