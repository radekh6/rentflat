<%--
  Created by IntelliJ IDEA.
  User: hulborad
  Date: 2015-09-15
  Time: 12:01
--%>

<%@ page import="eu.hulboj.Home" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="layout" content="welcomedec">
  <asset:javascript src="application.js"/>
</head>

<body>
    <!-- Jumbotron -->
    <div class="jumbotron">
        <h1>System rejestracji</h1>
        <p class="lead"><g:message code="page.welcome.main.info" default="System jest dla osób wynajmujacych mieszkanie. Właściciel Radoslaw H."/> </p>
        <p><a class="btn btn-lg btn-success" href="#" role="button"><g:message code="page.welcome.see.you" default="Warto zobaczyć"/></a></p>
        <p><g:generateLink/></p>
    </div>
</body>
</html>
