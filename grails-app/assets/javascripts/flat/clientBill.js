moduleBill = (function () {

    var selectorsContainer = {
        updatedSelector: "",
        removeTable: ""
    };

    var createBill = function (link, container) {
            updateLocalContainer (container);
            var data = getInputData();
            $.ajax({
                type: "POST",
                url: link,
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function (serverTemplateAsHtml) {
                    onSaveSuccess(serverTemplateAsHtml);
                },
                error: function (serverText) {
                    onSaveError(serverText);
                }
            })
        },
        onSaveSuccess = function (serverTemplateAsHtml) {
            $('#' + selectorsContainer.removeTable).remove();
            $("#" + selectorsContainer.updatedSelector).prepend($(serverTemplateAsHtml));
        },
        onSaveError = function (serverText) {
            console.log(serverText);
        },
        updateLocalContainer = function (container) {
            if(container) {
                selectorsContainer.updatedSelector = container.updatedSelector;
                selectorsContainer.removeTable = container.removeTable;
            }
        },
        getInputData = function () {
            return collectorBill.collectSelectors({water:0,gas:0, current1:0, current2:0});
        };


    return {
        createBill: createBill
    }
})();