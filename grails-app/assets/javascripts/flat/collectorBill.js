collectorBill = (function() {

    var collectSelectors = function(pMap) {
        for(var key in pMap) {
            if(pMap.hasOwnProperty(key)) {
                pMap[key] = $('#' + key).val();
            }
        }
        return pMap;
    };

    return {
        collectSelectors: collectSelectors
    }
})();