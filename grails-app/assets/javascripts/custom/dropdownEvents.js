// taken from this page -> http://jsfiddle.net/kcpma/18/
var dropdown = (function () {
    var initDropdown = function () {
        $('#userMenu').on({
            'show.bs.dropdown': function () {
            },
            "shown.bs.dropdown": function () {
                $('.dropdown-menu', this).removeClass('animated fadeInDown');
            },
            "hide.bs.dropdown": function () {
                var ile = $(this).text();
            },
            "hidden.bs.dropdown": function () {
                $('.dropdown-menu', this).removeClass('animated fadeOutDown');
                //alert('ni ca s-a terminat');
            }
        });
        $('#dropper li').on('click', function () {
            showSelectedTown($(this).text());
            tabPanel.showTabPanel();
        }),
        showSelectedTown = function (town) {
            var handler = $("#selectedTown");
            handler.text("Selected town: " + town);
        };
    }
    return {
        initDropdown: initDropdown
    }
})();