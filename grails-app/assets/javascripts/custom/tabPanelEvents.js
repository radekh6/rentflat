var tabPanel = (function () {
    var initTabPanel = function () {
        $("#tabPanelDiv").hide();
    },
    showTabPanel = function() {
         $("#tabPanelDiv").show();
    };
    return {
        initTabPanel: initTabPanel,
        showTabPanel: showTabPanel
    }
})();