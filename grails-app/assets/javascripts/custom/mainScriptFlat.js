/**
 * Created by hulborad on 2015-09-25.
 */

"use strict";


//= require dropdownEvents
//= require tabPanelEvents
//= require configurator

var initState = (function (){
    var initModule = function () {
        $("nav ul li").on("click", function () {
            $("nav ul li").removeClass("active");
            $(this).addClass("active");
        });
    };
    return {
        initModule: initModule
    }
})();

jQuery(document).ready(function() {

    dropdown.initDropdown();
    tabPanel.initTabPanel();
    initState.initModule();

});