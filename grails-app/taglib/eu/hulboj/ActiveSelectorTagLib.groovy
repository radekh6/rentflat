package eu.hulboj

import groovy.xml.MarkupBuilder


class ActiveSelectorTagLib {
	/*
		encode as text make working TagLib
	 */
   static defaultEncodeAs = [taglib:'text']
	
	def menuSelector = { attrs, body ->
		ParamService.MenuSelector menuSelector = attrs.selector
		String tagValue = attrs.value
		String controller = attrs.controller
		String action = attrs.action
		if (menuSelector == grailsApplication.mainContext.paramService.getCurrentSelector()) {
			out << "<li class =\"active\">"
			out << "<a href=" + g.createLink(controller: "${controller}", action: "${action}") + ">${tagValue}</a>"
			out << "</li>"
		} else {
			out << "<li>"
			out << "<a href=" + g.createLink(controller: "${controller}", action: "${action}") + ">${tagValue}</a>"
			out << "</li>"
		}
	}

	def generateLink = { attrs, body ->
		flash.message = "Wyswietl testowe nic"
		out << "<a href=" + g.createLink(controller: "flat", action: "welcome") + ">Flash</a>"
	}

	/**
	 * The markup builder doesn't work still: TODO: read about it and implement
	 */
	def testMenuSelector = {
		def mb = new MarkupBuilder(out)
		mb.div() {
			if (menuSelector == grailsApplication.mainContext.paramService.getCurrentSelector()) {
				out << "<li class =\"active\">"
				delegate.a(g.createLink(controller: "flatCalculator", action: "flat"))
				out << "</li>"
			} else {
				out << "<li>"
				delegate.a(g.createLink(controller: "flatCalculator", action: "flat", body: "${menuValue}"))
				out << "</li>"
			}
		}
	}
}
