package eu.hulboj
//static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

class GspFormaterTagLib {
    static defaultEncodeAs = [taglib: 'text']

    def spaceBr = { attrs, body ->
        String count = attrs?.count
        GString str = "${"<br/>".multiply(count.toInteger())}"
        out << str
    }
}
