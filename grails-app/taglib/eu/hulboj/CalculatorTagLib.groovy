package eu.hulboj

class CalculatorTagLib {
    //TODO: rahu encode HTML switch off

    //static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def renderToHtml = { attrs, body ->
        String resume = attrs?.calculator
        def size = resume?.split(',')?.size()
        resume?.split(',')?.eachWithIndex { iterator, index->
            if(index+1 != size)
                out << iterator + "</br>"
            else
                out << "<strong>${iterator}</strong>"
        }
    }
}
