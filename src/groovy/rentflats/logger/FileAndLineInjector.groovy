package rentflats.logger

import org.apache.log4j.AppenderSkeleton
import org.apache.log4j.MDC
import org.apache.log4j.spi.LoggingEvent

class FileAndLineInjector extends AppenderSkeleton {

    @Override
    public void close() {
    }

    @Override
    public boolean requiresLayout() {
        return false;
    }

    @Override
    protected void append(LoggingEvent event) {

        StackTraceElement[] strackTraceElements = Thread.currentThread().getStackTrace();

        StackTraceElement targetStackTraceElement = null;
        for (int i = 0; i < strackTraceElements.length; i++) {
            StackTraceElement strackTraceElement = strackTraceElements[i];
            if (strackTraceElement != null &&
                    strackTraceElement.declaringClass != null &&
                    strackTraceElement.declaringClass.startsWith("org.apache.commons.logging.Log\$") &&
                    i < (strackTraceElements.length - 1)) {
                targetStackTraceElement = strackTraceElements[++i];
                while (targetStackTraceElement.declaringClass != null &&
                        targetStackTraceElement.declaringClass.startsWith("org.codehaus.groovy.runtime.callsite.") &&
                        i < (strackTraceElements.length - 1)) {
                    targetStackTraceElement = strackTraceElements[++i];
                }
                break;
            }
        }

        if (targetStackTraceElement != null) {
            MDC.put("fileAndLine", "(" + targetStackTraceElement.getFileName() + ":" + targetStackTraceElement.getLineNumber() + ")");
        } else {
            MDC.remove("fileAndLine");
        }
    }
}