package rentflats.builders

import eu.hulboj.config.PayType
import eu.hulboj.config.ServiceType
import rentflats.database.HomeDatabaseService
import rentflats.json.BuildRecord
import rentflats.json.ImplBuildRecord

/**
 * Created by radoslaw on 05.10.16.
 */
class FactoryRecordBuilder {
    HomeDatabaseService homeDatabaseService

    List<Map<Map, BuildRecord>> build(def json) {
        def returnList = []
        buildGas(json, returnList)
        buildWater(json, returnList)
        buildCurrent1(json, returnList)
        buildCurrent2(json, returnList)
        returnList
    }

    private void buildCurrent2(json, ArrayList returnList) {
        if (json?.current2 != null) {
            def mapValue = buildCurrent2MapValue(json)
            returnList << buildMapHolder(mapValue)
        }
    }

    private void buildCurrent1(json, ArrayList returnList) {
        if (json?.current1 != null) {
            def mapValue = buildCurrent1MapValue(json)
            returnList << buildMapHolder(mapValue)
        }
    }

    private void buildWater(json, ArrayList returnList) {
        if (json?.water != null) {
            def mapValue = buildWaterMapValue(json)
            returnList << buildMapHolder(mapValue)
        }
    }

    private void buildGas(json, ArrayList returnList) {
        if (json?.gas != null) {
            def mapValue = buildGasMapValue(json)
            returnList << buildMapHolder(mapValue)
        }
    }

    private def buildCurrent2MapValue(json) {
        LinkedHashMap mapValue = [:]
        mapValue.providerInfo = getHomeDatabaseService().getProviderInfo(PayType.CURRENT_NIGHT, ServiceType.CURRENT)
        mapValue.payInfo = getHomeDatabaseService().getPayInfo(PayType.CURRENT_NIGHT)
        mapValue.value = json.current2
        mapValue
    }

    private def buildCurrent1MapValue(json) {
        LinkedHashMap mapValue = [:]
        mapValue.providerInfo = getHomeDatabaseService().getProviderInfo(PayType.CURRENT_DAY, ServiceType.CURRENT)
        mapValue.payInfo = getHomeDatabaseService().getPayInfo(PayType.CURRENT_DAY)
        mapValue.value = json.current1
        mapValue
    }

    private def buildWaterMapValue(json) {
        LinkedHashMap mapValue = [:]
        mapValue.providerInfo = getHomeDatabaseService().getProviderInfo(PayType.WATER_QUANTITY, ServiceType.WATER)
        mapValue.payInfo = getHomeDatabaseService().getPayInfo(PayType.WATER_QUANTITY)
        mapValue.value = json.water
        mapValue
    }

    private def buildGasMapValue(json) {
        LinkedHashMap mapValue = [:]
        mapValue.providerInfo = getHomeDatabaseService().getProviderInfo(PayType.GAS_QUANTITY, ServiceType.GAS)
        mapValue.payInfo = getHomeDatabaseService().getPayInfo(PayType.GAS_QUANTITY)
        mapValue.value = json.gas
        mapValue
    }

    private LinkedHashMap buildMapHolder(LinkedHashMap mapValue) {
        LinkedHashMap mapHolder = [:]
        mapHolder.map = mapValue
        mapHolder.buildRecord = new ImplBuildRecord()
        mapHolder
    }

    private def getHomeDatabaseService() {
        if(!homeDatabaseService)
            homeDatabaseService = grails.util.Holders.grailsApplication.mainContext.getBean 'homeDatabaseService'
        homeDatabaseService
    }

}
