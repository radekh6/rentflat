package rentflats.json

import eu.hulboj.form.Record

/**
 * Created by radoslaw on 05.10.16.
 */
interface BuildRecord {

    Record buildRecordFrom(Map providerInfoPayInfoMap)
}