package rentflats.json

import eu.hulboj.form.Record

/**
 * Created by radoslaw on 05.10.16.
 */
class ImplBuildRecord implements rentflats.json.BuildRecord {
    @Override
    Record buildRecordFrom(Map recordInfo) {
        new Record(providerInfo: recordInfo.providerInfo, payInfo: recordInfo.payInfo, value: recordInfo.value)
    }
}
